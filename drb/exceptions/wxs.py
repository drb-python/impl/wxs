from drb.exceptions.core import DrbException


class WXsRequestException(DrbException):
    pass
