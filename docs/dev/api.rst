.. _api:

Reference API
=============

WXSServiceNode
---------------
.. autoclass:: drb.drivers.wxs.wXs_node.WXSServiceNode
    :members:

WXSNodeOperation
---------------------
.. autoclass:: drb.drivers.wxs.wXs_node.WXSNodeOperation
    :members:

WXSNodeOperationGetCapabilities
-----------
.. autoclass:: drb.drivers.wxs.wXs_node.WXSNodeOperationGetCapabilities
    :members:
