===================
Data Request Broker
===================
---------------------------------
Web Service OGC driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-wXs/month
    :target: https://pepy.tech/project/drb-driver-wXs
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-wXs.svg
    :target: https://pypi.org/project/drb-driver-wXs/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-wXs.svg
    :target: https://pypi.org/project/drb-driver-wXs/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-wXs.svg
    :target: https://pypi.org/project/drb-driver-wXs/
    :alt: Python Version Support Badge

-------------------

This drb-driver-wxs module implements access to wxs containers with DRB data model.
It is able to navigates among the wxs contents.


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api



